package com.cache.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.cache.module.Amazon;
import com.cache.services.AmazonCacheService;

@RestController
public class AmazonCacheController {
	@Autowired
	private AmazonCacheService amazonCacheService;

	@GetMapping(value = "/amazon")
	public ResponseEntity<List<Amazon>> getAmazonDetails() {
		List<Amazon> Amazon = amazonCacheService.getAmazonDetails();

		return ResponseEntity.status(200).body(Amazon);
	}

	@PostMapping(value = "/amazon")
	public ResponseEntity<Amazon> saveAmazon(@RequestBody Amazon Amazon) {
		Amazon amazon = amazonCacheService.saveAmazon(Amazon);
		return ResponseEntity.status(200).body(amazon);
	}

	@PutMapping(value = "/amazon/{id}")
	public ResponseEntity<Amazon> updateTicket(@PathVariable("id") Integer id, @RequestBody Amazon amazon) {
		amazon.setId(id);
		Amazon amazon1 = amazonCacheService.updateAmazon(amazon);
		return ResponseEntity.status(200).body(amazon1);
	}
}
