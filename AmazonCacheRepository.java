package com.cache.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cache.module.Amazon;

@Repository
public interface AmazonCacheRepository extends JpaRepository<Amazon, Integer> {

}
