package com.cache.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.stereotype.Service;

import com.cache.module.Amazon;
import com.cache.repository.AmazonCacheRepository;

@Service
public class AmazonCacheService {
	@Autowired
	private AmazonCacheRepository amazonCacheRepository;

	public Amazon saveAmazon(Amazon amazon) {
		return amazonCacheRepository.save(amazon);
	}

	@org.springframework.cache.annotation.Cacheable
	public List<Amazon> getAmazonDetails() {
		try {
			System.out.println("Going to sleep for 5 Secs.. to simulate backend call.");
			Thread.sleep(100 * 5);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return amazonCacheRepository.findAll();
	}

	@CachePut(value = "amazons", key = "#amazon.id")
	public Amazon updateAmazon(Amazon amazon) {
		return amazonCacheRepository.saveAndFlush(amazon);
	}
}
