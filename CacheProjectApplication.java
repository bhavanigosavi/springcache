package com.cache.module;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import io.swagger.annotations.SwaggerDefinition;

@EnableCaching
@SpringBootApplication(scanBasePackages = "com")
@EnableJpaRepositories(basePackages = "com.cache.repository")
public class CacheProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(CacheProjectApplication.class, args);
	}

}
